# ansible-downloader

Role for bootstrapping installer. 

First of all this repository is cloned.
Second we clone inventory repository.
Third we run:

```
ansible-playbook load_installer.yml -i /path/to/product/inventory -e @/path/to/product/inventory/extra_vars.yml
```

Role simplified for working with git only.

Role takes several parameters from product inventory:

- `artifact.path` - url to git repository
- `artifact.version` - git branch in repository

In our example paremeters are stored in `https://gitlab.com/charon_deployer/sample-inventory-2/blob/master/extra_vars.yml`. Jenkins will pass all file as extra_vars to ansible-playbook.

Includes pre and post hooks. Example shows using hooks for merging default ansible.cfg (this repository) and ansible.cfg from artifact.