import ast
def format_list(list_):
    return ast.literal_eval(list_)


class FilterModule(object):
    def filters(self):
        return {
            'format_list': format_list,
        }
